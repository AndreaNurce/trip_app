import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as JWT from 'jsonwebtoken';

@Injectable()
export class AgencyMiddleware implements NestMiddleware {
  async use(req: Request, res: Response, next: NextFunction) {
    let token = req.headers?.authorization;
    if (token) {
      await JWT.verify(
        token,
        process.env.JWT_SECRET,
        function (err: Error, decoded) {
          if (err)
            throw new UnauthorizedException('I dont know you please leave!');
          if (decoded.data.type !== 'agency')
            throw new ForbiddenException('Method not allowed! Please , leave!');
          req.body.agency = decoded.data._id
        },
      );
    } else {
      throw new UnauthorizedException('I dont know you please leave!');
    }
    next();
  }
}
