import { userStub } from 'src/user/test/stubs/user.stub';
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzUzNTU0MTgsImRhdGEiOnsiX2lkIjoiNjE3NjU3NWQ0MjgwYTY0Njk1N2RkMzY4IiwidHlwZSI6InVzZXIiLCJuYW1lIjoiYW5kcmVhbnVyY2UifSwiaWF0IjoxNjM1MzUxODE4fQ.BcWB8lu0qXHZJFX2SyUy-xwUf-VJlkVZeErTluLLiGE';

export const AuthServiceMock = jest.fn().mockReturnValue({
  registerNewAgency: jest.fn().mockResolvedValue(userStub()),
  registerNewUser: jest.fn().mockResolvedValue(userStub()),
  signInUser: jest.fn().mockResolvedValue(token),
  signInAgency: jest.fn().mockResolvedValue(token),
});
