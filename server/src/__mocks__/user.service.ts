import { tripStub } from "src/trips/test/stabs/trips.stub";


export  const UserService  = jest.fn().mockReturnValue({

    getAllTrips : jest.fn().mockResolvedValue([tripStub()])

})