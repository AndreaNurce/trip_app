import { tripStub } from "src/trips/test/stabs/trips.stub";

export const TripService = jest.fn().mockReturnValue({
  getAllAvailableTrips: jest.fn().mockResolvedValue([tripStub()]),
  getTripById: jest.fn().mockResolvedValue(tripStub()),
});
