import { placedOrderStub } from "src/order/stubs/order.stubs";



export  const OrderService  = jest.fn().mockReturnValue({

    findOrders : jest.fn().mockResolvedValue([placedOrderStub()])

})