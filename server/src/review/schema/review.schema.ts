import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type ReviewDocument = Review & mongoose.Document;

@Schema()
export class Review {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User'})
  user_id: [mongoose.Schema.Types.ObjectId];

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'Agency', default: [] })
  agency_id: [mongoose.Schema.Types.ObjectId];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Trips' })
  trip_id: mongoose.Schema.Types.ObjectId;
}

export const ReviewSchema = SchemaFactory.createForClass(Review);
