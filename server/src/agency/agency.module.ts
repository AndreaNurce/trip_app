import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AgencyMiddleware } from 'src/middlewares/agency.middleware';
import { Review, ReviewSchema } from 'src/review/schema/review.schema';
import { Trip, TripSchema } from 'src/trips/schema/trip.schema';
import { TripService } from 'src/trips/trips.service';
import { User, UserSchema } from 'src/user/schema/user.schema';
import { AgencyController } from './agency.controller';
import { AgencyService } from './agency.service';
import { Agency, AgencySchema } from './schema/agency.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Agency.name, schema: AgencySchema },
      { name: User.name, schema: UserSchema },
      { name: Trip.name, schema: TripSchema },
      { name: Review.name, schema: ReviewSchema },
    ]),
  ],
  controllers: [AgencyController],
  providers: [AgencyService, TripService],
})
export class AgencyModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AgencyMiddleware).forRoutes(AgencyController);
  }
}
