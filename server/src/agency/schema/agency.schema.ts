import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type AgencyDocument = Agency & mongoose.Document;

@Schema()
export class Agency {
  @Prop()
  name: string;

  @Prop({ unique: true, lowercase: true })
  email: string;

  @Prop()
  password: string;

  @Prop()
  phone: string;
}

export const AgencySchema = SchemaFactory.createForClass(Agency);
