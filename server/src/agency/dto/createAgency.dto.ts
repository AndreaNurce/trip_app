import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class CreateAgencyDto {
  @Length(3, 20)
  name: string;

  @IsEmail()
  email: string;

  @Length(3, 20)
  password: string;

  @Length(10, 20)
  phone?: string | number;
}
