import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Agency, AgencyDocument } from './schema/agency.schema';
@Injectable()
export class AgencyService {
  constructor(
    @InjectModel(Agency.name) private agencyDocument: Model<AgencyDocument>,
  ) {}
}
