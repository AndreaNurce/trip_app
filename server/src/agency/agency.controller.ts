import { Body, Controller, Get, Post, Delete, Param } from '@nestjs/common';
import { AgencyService } from './agency.service';
import { TripService } from 'src/trips/trips.service';
import { CreateTripDto } from 'src/trips/dto/create-trip.dto';

@Controller('agency')
export class AgencyController {
  constructor(
    private agencyService: AgencyService,
    private tripService: TripService,
  ) {}

  @Post('trip')
  createNewTrip(@Body() body: CreateTripDto) {
    return this.tripService.createNewTrip(body);
  }
  @Post('/trip/:id')
  updateTrip(@Body() body: CreateTripDto, @Param() param) {
    return this.tripService.updateTrip(body, param.id);
  }
  @Get('/trip/:id')
  getTripById(@Param() param) {
    return this.tripService.getTripById(param.id);
  }
  @Get('all-trips')
  getAllAgencyTrips(@Body() agency) {
    return this.tripService.getAllAgencyTrips(agency);
  }
  @Get('available-trips')
  getAvailableTrips(@Body() agency) {
    return this.tripService.getAvailableTrips(agency);
  }
  @Delete('trip')
  deleteAgencyTrip(@Body() body) {
    return this.tripService.deleteAgencyTrip(body.agency, body.tripId);
  }
}
