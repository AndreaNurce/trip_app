import { IsEmail, IsNotEmpty, Length} from 'class-validator';

export class AuthUser {
    
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @Length(3, 20)
  @IsNotEmpty()
  password: string;
}
