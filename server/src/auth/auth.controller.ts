import {
  Body,
  UnauthorizedException,
  Controller,
  Get,
  Post,
} from '@nestjs/common';
import { CreateAgencyDto } from 'src/agency/dto/createAgency.dto';
import { CreateUserDto } from 'src/user/dto/createUser.dto';

import { AuthService } from './auth.service';
import { AuthUser } from './dto/auth-user.dto';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup/agency')
  async registerNewAgency(@Body() agency: CreateAgencyDto) {
    return await this.authService.registerNewAgency(agency);
  }
  @Post('/signup/user')
  async registerNewUser(@Body() user: CreateUserDto) {
    return await this.authService.registerNewUser(user);
  }

  @Post('login/user')
  async signInUser(@Body() credentials: AuthUser) {
    return await this.authService.signInUser(credentials);
  }
  @Post('login/agency')
  async signInAgency(@Body() credentials: AuthUser) {
    return await this.authService.signInAgency(credentials);
  }
}
