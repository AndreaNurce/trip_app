import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AuthUser } from './dto/auth-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/user/schema/user.schema';
import { CreateAgencyDto } from 'src/agency/dto/createAgency.dto';
import { Agency, AgencyDocument } from 'src/agency/schema/agency.schema';
import { CreateUserDto } from 'src/user/dto/createUser.dto';
import * as JWT from 'jsonwebtoken';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private userDocument: Model<UserDocument>,
    @InjectModel(Agency.name) private agencyDocument: Model<AgencyDocument>,
  ) {}

  async hashPass(password: string): Promise<string> {
    return await bcrypt.hash(password, +process.env.SALT_ROUNDS);
  }
  async compareHashes(password: string, hash: string) {
    return await bcrypt.compare(password, hash);
  }
  async signUserToken(data: any) {
    return JWT.sign(
      { exp: Math.floor(Date.now() / 1000) + 60*60, data },
      process.env.JWT_SECRET,
    );
  }

  async registerNewAgency(agency: CreateAgencyDto) {
    let emailFound = await this.agencyDocument.findOne({ email: agency.email });
    if (emailFound != null)
      throw new ConflictException('Email already in use!');

    let newAgency = new this.agencyDocument(agency);
    newAgency.password = await this.hashPass(agency.password);
    newAgency.save();

    let { password, ...data } = agency;
    return data;
  }

  async registerNewUser(user: CreateUserDto) {
    let emailFound = await this.userDocument.findOne({ email: user.email });
    if (emailFound != null)
      throw new ConflictException('Email already in use!');

    let newUser = new this.userDocument(user);
    newUser.password = await this.hashPass(user.password);
    await newUser.save();

    let { password, ...data } = user;
    return data;
  }

  async signInUser(credentials: AuthUser) {
    let user = await this.userDocument.findOne({ email: credentials.email });
    if (
      user == null ||
      !(await this.compareHashes(credentials.password, user.password))
    )
      throw new BadRequestException('Invalid email or password');

    return this.signUserToken({ _id: user._id, type: 'user', name: user.name });
  }
  async signInAgency(credentials: AuthUser) {
    let agency = await this.agencyDocument.findOne({
      email: credentials.email,
    });
    if (
      agency == null ||
      !(await this.compareHashes(credentials.password, agency.password))
    )
      throw new BadRequestException('Invalid email or password');

    return this.signUserToken({
      _id: agency._id,
      type: 'agency',
      name: agency.name,
    });
  }
}
