import { Test } from '@nestjs/testing';
import { AuthService } from '../auth.service';
import { AuthServiceMock } from 'src/__mocks__/auth.service';
import { AuthController } from '../auth.controller';
import { User } from 'src/user/schema/user.schema';
import { userStub } from 'src/user/test/stubs/user.stub';

describe('UserController', () => {
  let tokenRegx = new RegExp('');
  let authController: AuthController;
  let authService: AuthService;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useClass: AuthServiceMock,
        },
      ],
    }).compile();

    authController = moduleRef.get<AuthController>(AuthController);
    authService = moduleRef.get<AuthService>(AuthService);
    jest.clearAllMocks();
  });
  it('should be defined', () => {
    expect(AuthController).toBeDefined();
  });

  describe('registerNewAgency', () => {
    describe('When registerNewAgency is called ', () => {
      let user;

      beforeAll(async () => {
        user = await authController.registerNewAgency(userStub());
      });

      it('then it should call registerNewAgency', () => {
        expect(authService.registerNewAgency).toBeCalled();
      });

      it('then it should return a user', () => {
        expect(user).toMatchObject(userStub());
      });
    });
  });
  describe('registerNewUser', () => {
    describe('When registerNewUser is called ', () => {
      let user;

      beforeAll(async () => {
        user = await authController.registerNewUser(userStub());
      });

      it('then it should call registerNewUser', () => {
        expect(authService.registerNewUser).toBeCalled();
      });

      it('then it should return a user', () => {
        expect(user).toMatchObject(userStub());
      });
    });
  });

  describe('signInUser', () => {
    describe('When signInUser is called ', () => {
      let token: string;

      beforeAll(async () => {
        token = await authController.signInUser(userStub());
      });

      it('then it should call registerNewAgency', () => {
        expect(authService.signInUser).toBeCalled();
      });

      it('then it should return a user', () => {
        expect(token).toMatch(
          /^([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_\-\+\/=]*)/,
        );
      });
    });
  });
  describe('signInAgency', () => {
    describe('When signInAgency is called ', () => {
      let token: string;

      beforeAll(async () => {
        token = await authController.signInAgency(userStub());
      });

      it('then it should call signInAgency', () => {
        expect(authService.signInAgency).toBeCalled();
      });

      it('then it should return a user', () => {
        expect(token).toMatch(
          /^([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_\-\+\/=]*)/,
        );
      });
    });
  });
});
