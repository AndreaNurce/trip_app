import { IsNotEmpty } from 'class-validator';
export class CreateReviewDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  rate: number;

  @IsNotEmpty()
  trip_id: string;

  @IsNotEmpty()
  user_id: string;
}
