import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import { Timestamp } from 'typeorm';

export class CreateTripDto {
    @IsNotEmpty()
    description: string;
  
    @IsNotEmpty()
    title: string;
  
    @IsNotEmpty()
    destination: string;
  
    @IsNotEmpty()
    maxNumber: number;
  
    @IsNotEmpty()
    endDate: Timestamp;
  
    @IsNotEmpty()
    startDate: Timestamp;
}






