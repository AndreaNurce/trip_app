import { ConsoleLogger, forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Order, OrderDocument } from 'src/order/schema/order.schema';
import { CreateTripDto } from './dto/create-trip.dto';
import { Trip, TripDocument } from './schema/trip.schema';

@Injectable()
export class TripService {
  constructor(
    @InjectModel(Trip.name)
    private tripDocument: Model<TripDocument>,
  ) {}
  async createNewTrip(trip: CreateTripDto) {
    let newTrip = new this.tripDocument(trip);
    newTrip.save();
    return newTrip;
  }
  async getAllAgencyTrips(agency) {
    return await this.tripDocument
      .find({ agency: agency.agency })
      .sort({ _id: -1 })
      .populate('agency', '-password -__v');
  }
  async getAvailableTrips(agency) {
    return await this.tripDocument
      .find({ agency: agency.agency, startDate: { $gte: Date.now() } })
      .sort({ _id: -1 })
      .populate('agency', '-password -__v');
  }
  async deleteAgencyTrip(agency, id: ObjectId) {
    return await this.tripDocument.findOneAndDelete({
      agency: agency,
      _id: id,
    });
  }
  async updateTrip(data: object, id: ObjectId) {
    return await this.tripDocument.findByIdAndUpdate(id, data, { new: true });
  }
  async getTripById(_id: ObjectId) {
    return await this.tripDocument
      .findById(_id)
      .populate('reviews.user_id', '-password -__v')
  }
  async getAllAvailableTrips() {
    return await this.tripDocument
      .find({ startDate: { $gte: Date.now() } })
      .sort({ _id: -1 })
      .populate('agency', '-password -__v');
  }

  async addParticipant(trip_id: string) {
    return await this.tripDocument.findByIdAndUpdate(
      { _id: trip_id },
      { $inc: { participants: 1 } }
    );
  }

  async removeParticipant(trip_id: string) {
    return await this.tripDocument.findByIdAndUpdate(
      { _id: trip_id },
      { $inc: { participants: -1 } }
    );
  }

  async addReview(body) {
    let { trip_id, ...rest } = body;
    await this.tripDocument.findByIdAndUpdate(body.trip_id, {
      $push: { reviews: rest },
    });
  }
}
