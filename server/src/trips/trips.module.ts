import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Order, OrderSchema } from 'src/order/schema/order.schema';
import { User, UserSchema } from 'src/user/schema/user.schema';
import { Trip, TripSchema } from './schema/trip.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Trip.name, schema: TripSchema },
      { name: Order.name, schema: OrderSchema },
    ]),
  ],
  controllers: [],
  providers: [],
})
export class TripModule {}
