import { Trip } from '../../schema/trip.schema';
import * as mongoose from "mongoose"

export const tripStub = (): Trip => {
  return {
    description: 'this is a description for the current trip',
    title: 'tittle',

    destination: 'London',

    maxNumber: 20,

    endDate: 12345678,

    startDate: 12345678,

    price: 2102,

    participants: 1,

    reviews: [
      {
        title: 'Good trip',
        rate: 5,
        description: 'very good trip',
        user_id: new mongoose.Schema.Types.ObjectId('6176575d4280a646957dd368'),
      },
    ],

    agency: new mongoose.Schema.Types.ObjectId('6176d390db6d7b9696920e6d'),
  };
};
