import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type TripDocument = Trip & mongoose.Document;

@Schema()
export class Trip {
  @Prop()
  description: string;

  @Prop()
  title: string;

  @Prop()
  destination: string;

  @Prop()
  maxNumber: number;

  @Prop()
  endDate: number;

  @Prop()
  startDate: number;

  @Prop()
  price: number;

  @Prop({ default: 0 })
  participants: number;

  @Prop({
    type: [
      {
        title: { type: String },
        rate: { type: Number },
        description: { type: String },
        user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        created_at: { type: Number, default: Date.now() },
      },
    ],
    default: [],
  })
  reviews: [{}];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Agency' })
  agency: mongoose.Schema.Types.ObjectId;
}

export const TripSchema = SchemaFactory.createForClass(Trip);
