import { IsEmail, IsNotEmpty, Length, Min } from 'class-validator';
export class PlaceOrderDto {
  @IsNotEmpty()
  user_id: string;

  @IsNotEmpty()
  trip_id: string;

  hasPayed : boolean;


}
