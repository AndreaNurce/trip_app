export const placedOrderStub = () => {
  return {
    agency: "6176d390db6d7b9696920e6d",
    agency_id: "6176d390db6d7b9696920e6d",
    description: "sdfgsdfgsdfgsdfgsdfg",
    destination: "England",
    endDate: 1635638400000,
    hasPayed: false,
    maxNumber: 34,
    order_id: "6178f1eb9e6fe8ed7508ac3e",
    participants: 1,
    placedAt: 1635316020352,
    price: 34,
    reviews: [],
    startDate: 1635379200000,
    title: "new trip",
    user_id: "6176575d4280a646957dd368",
    __v: 0,
    _id: "6178f1d49e6fe8ed7508ac38",
  };
};