import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { TripService } from 'src/trips/trips.service';
import { Order, OrderDocument } from './schema/order.schema';
import * as mongoose from 'mongoose';
@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name) private orderDocument: Model<OrderDocument>,
    private readonly tripService: TripService,
  ) {}

  async orderAlreadyPlaced(user_id, trip_id) {
    return await this.orderDocument.findOne({ user_id, trip_id });
  }

  async findOrder(_id) {
    return await this.orderDocument.findOne({ _id });
  }

  async placeOrder(body) {
    if (await this.orderAlreadyPlaced(body.user_id, body.trip_id)) {
      throw new ConflictException('Order alaready placed!');
    }
    try {
      let order = new this.orderDocument(body);
      await order.save();
      await this.tripService.addParticipant(body.trip_id);
      return 'Order saved succesfuly';
    } catch (err) {
      console.log({ err });
      throw new BadRequestException('Something went wrong');
    }
  }

  async cancelOrder(order_id, trip_id) {
    if (!(await this.findOrder(order_id))) {
      throw new BadRequestException(
        'Something went wrong. Action was not completed!',
      );
    }
    try {
      await this.orderDocument.findByIdAndRemove(order_id);
      await this.tripService.removeParticipant(trip_id);
      return 'Order deleted succesfuly';
    } catch (err) {
      console.log({ err });
      throw new BadRequestException(
        'Something went wrong. Action was not completed!',
      );
    }
  }

  async findOrders(user_id) {
    user_id = new mongoose.Types.ObjectId(user_id);
    return await this.orderDocument.aggregate([
      {
        $match: {
          user_id: user_id,
        },
      },
      {
        $lookup: {
          from: 'trips',
          localField: 'trip_id',
          foreignField: '_id',
          as: 'agency',
        },
      },
      {
        $set: {
          agency: { $first: '$agency' },
          order_id: '$_id',
        },
      },
      {
        $replaceRoot: {
          newRoot: { $mergeObjects: ['$$ROOT', '$agency'] },
        },
      },
      {
        $project: {
          trip_id: 0,
        },
      },
    ]);
  }
}
