import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type OrderDocument = Order & mongoose.Document;

@Schema()
export class Order {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user_id: [mongoose.Schema.Types.ObjectId];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Trip' })
  trip_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Agency' })
  agency_id: mongoose.Schema.Types.ObjectId;

  @Prop({ default: false })
  hasPayed: Boolean;

  @Prop({ default: Date.now() })
  placedAt: number;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
