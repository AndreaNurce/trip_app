import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AgencyModule } from './agency/agency.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.${process.env.NODE_ENV}`],
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.MONGODB_URL, {
      // retry to connect for 60 times
      retryAttempts: 60,
      // wait 1 second before retrying
      retryDelay: 1000,
    }),
    UserModule,
    AgencyModule,
    AuthModule,
  ],
})
export class AppModule {}
