import { User } from 'src/user/schema/user.schema';

export const userStub = (): User => {
  return {
    name: 'andreanurce',

    email: 'andrea@gmail.com',

    password: '123412341234',

    phone: '123412341234',
  };
};