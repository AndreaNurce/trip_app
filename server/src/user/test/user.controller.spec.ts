import { Test } from '@nestjs/testing';

import { UserController } from '../user.controller';
import { TripService } from 'src/trips/trips.service';
import { Trip } from 'src/trips/schema/trip.schema';
import { tripStub } from 'src/trips/test/stabs/trips.stub';
import { TripService as TripServiceMock } from 'src/__mocks__/trips.service';

import { OrderService } from 'src/order/order.service';
import { OrderService as OrderServiceMock } from 'src/__mocks__/order.service'; 
import { placedOrderStub } from 'src/order/stubs/order.stubs';

jest.mock('../user.service.ts');
jest.mock('../../trips/trips.service.ts');

describe('UserController', () => {
  let userController: UserController;
  let tripService: TripService;
  let orderService: OrderService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: OrderService,
          useClass: OrderServiceMock,
        },
        {
          provide: TripService,
          useClass: TripServiceMock,
        },
      ],
    }).compile();

    userController = moduleRef.get<UserController>(UserController);
    tripService = moduleRef.get<TripService>(TripService);
    orderService = moduleRef.get<OrderService>(OrderService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(userController).toBeDefined();
  });
  describe('getAllTrips', () => {
    describe('When get allTrips is called ', () => {
      let trips: Trip[];

      beforeAll(async () => {
        trips = await userController.getAllTrips();
      });

      it('then it should call tripservice', () => {
        expect(tripService.getAllAvailableTrips).toBeCalled();
      });

      it('then it should return list of trips', () => {
        expect(trips).toStrictEqual([tripStub()]);
      });
    });
  });

  describe('getUserTrips', () => {
    let user_id = '6176d390db6d7b9696920e6d';

    describe('When get allTrips is called ', () => {
      let placedOrders;

      beforeAll(async () => {
        placedOrders = await userController.getUserTrips(user_id);
      });

      it('then it should call tripservice', () => {
        expect(orderService.findOrders).toBeCalled();
      });

      it('then it should return list of trips', () => {
        expect(placedOrders).toStrictEqual([placedOrderStub()]);
      });
    });
  });
  describe('getTripById', () => {
    let user_id = '6176d390db6d7b9696920e6d';
    describe('When getTripById is called ', () => {
      let trip: Trip;

      beforeAll(async () => {
        trip = await userController.getTripById(user_id);
      });

      it('then tripService.getTripById should be called ', () => {
        expect(tripService.getTripById).toBeCalled();
      });

      it('then it should return  a signle  trips', () => {
        expect(trip).toStrictEqual(tripStub());
      });
    });
  });
});
