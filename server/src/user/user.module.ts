import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserMiddleware } from 'src/middlewares/user.middleware';
import { OrderModule } from 'src/order/order.module';
import { OrderService } from 'src/order/order.service';
import { Order, OrderSchema } from 'src/order/schema/order.schema';
import { Trip, TripSchema } from 'src/trips/schema/trip.schema';
import { TripService } from 'src/trips/trips.service';
import { User, UserSchema } from './schema/user.schema';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Trip.name, schema: TripSchema },
      { name: Order.name, schema: OrderSchema },
    ]),
  ],
  controllers: [UserController],
  providers: [UserService, TripService, OrderService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(UserMiddleware).forRoutes(UserController);
  }
}
