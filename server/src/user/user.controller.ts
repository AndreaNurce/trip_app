import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { PlaceOrderDto } from 'src/order/dto/place-order.dto';
import { OrderService } from 'src/order/order.service';
import { ReviewService } from 'src/review/review.service';
import { CreateReviewDto } from 'src/trips/dto/create-review.dto';
import { TripService } from 'src/trips/trips.service';
import { UserService } from './user.service';

@Controller('/user')
export class UserController {
  constructor(
    private tripService: TripService,
    private orderService: OrderService,
  ) {}

  @Get('/')
  getAllTrips() {
    return this.tripService.getAllAvailableTrips();
  }
  @Get('/trips')
  async getUserTrips(@Body() body) {
    return await this.orderService.findOrders(body.user_id);
  }

  @Get('/trip/:id')
  async getTripById(@Param() params) {
    return await this.tripService.getTripById(params._id);
  }

  @Delete('/cancel-trip')
  async cancelTrip(@Body() body) {
    return await this.orderService.cancelOrder(body.order_id, body.trip_id);
  }

  @Post('/participate')
  placeOrder(@Body() body: PlaceOrderDto) {
    return this.orderService.placeOrder(body);
  }

  @Post('/review')
  addReview(@Body() body: CreateReviewDto) {
    return this.tripService.addReview(body);
  }
}
