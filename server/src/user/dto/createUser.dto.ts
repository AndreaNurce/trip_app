import { IsEmail, IsNotEmpty, Length, Min } from 'class-validator';
export class CreateUserDto {
  @Length(3, 20)
  name: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(8, 20)
  password: string;

  @IsNotEmpty()
  phone: string | number;
}
