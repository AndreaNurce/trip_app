import { Model } from 'mongoose';
import {
  Injectable,
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schema/user.schema';
import { Order, OrderDocument } from 'src/order/schema/order.schema';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userDocument: Model<UserDocument>,
  ) {}
}
