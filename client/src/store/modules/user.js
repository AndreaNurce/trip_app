import ApiService from "../../services/api.service";

const state = {
  trips: [],
};

const getters = {
  getTrips: (state) => state.trips,
};

const mutations = {
  setTrips: (state, value) => {
    state.trips = value;
  },
  removeTrip: (state, id) => {
    state.trips = state.trips.filter((i) => i.order_id !== id);
  },
};

const actions = {
  async fetchAllTrips({ commit }) {
    try {
      let { data } = await ApiService.get("/user");
      commit("setTrips", data);
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async fetchTrips({ commit }) {
    try {
      let { data } = await ApiService.get("/user/trips");
      commit("setTrips", data);
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async cancelTrip({ commit }, { order_id, trip_id }) {
    try {
      let { data } = await ApiService.delete("/user/cancel-trip", {
        order_id,
        trip_id,
      });
      commit("removeTrip", order_id);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: "Action completed successfylly",
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async participate({ commit }, payload) {
    try {
      let { data } = await ApiService.post("/user/participate", payload);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: data,
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },

  async fetchTripById({ commit }, _id) {
    try {
      return (await ApiService.post(`/user/trip/${_id}`)).data;
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },

  async submitReview({ commit }, payload) {
    try {
      await ApiService.post("/user/review", payload);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: "Review completed successfully",
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
