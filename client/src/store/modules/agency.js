import ApiService from "../../services/api.service";

const state = {
  trips: [],
};

const getters = {
  getTrips: (state) => state.trips,
};

const mutations = {
  setTrips: (state, value) => {
    state.trips = value;
  },
  addTrip: (state, value) => {
    state.trips.unshift(value);
  },
  removeTrip: (state, id) => {
    state.trips = state.trips.filter((i) => i._id !== id);
  },
  updateTrip: (state, data) => {
    for (let i = 0; i < state.trips.length; i++) {
      if (state.trips[i]._id === data._id) {
        state.trips[i] = data;
      }
    }
  },
};

const actions = {
  async fetchAllTrips({ commit }) {
    try {
      let { data } = await ApiService.get("/agency/all-trips");
      commit("setTrips", data);
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async fetchAvailableTrips({ commit }) {
    try {
      let { data } = await ApiService.get("/agency/available-trips");
      commit("setTrips", data);
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async createNewTrip({ commit }, payload) {
    try {
      let { data } = await ApiService.post("/agency/trip", payload);
      commit("addTrip", data);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: "Trip created successfully",
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async deleteTrip({ commit }, tripId) {
    try {
      await ApiService.delete("/agency/trip", { tripId });
      commit("removeTrip", tripId);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: "Trip deleted succesfully",
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async updateTrip({ commit }, payload) {
    try {
      let { tripId, ...rest } = payload;
      let { data } = await ApiService.post(`/agency/trip/${tripId}`, rest);
      commit("updateTrip", data);
      commit(
        "notifications/addNotification",
        {
          type: "success",
          message: "Trip updated successfully",
        },
        { root: true }
      );
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async fetchTripById({ commit }, _id) {
    try {
      return (await ApiService.get(`/agency/trip/${_id}`)).data
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
