const state = {
  notifications: [],
};

const getters = {
  getNotifications: (state) => state.notifications,
};

const mutations = {
  removeNotification: (state, notification) => {
    state.notifications = state.notifications.filter(
      (i) => i._id !== notification._id
    );
  },
  addNotification: (state, notification) => {
    state.notifications.unshift({
      _id: Date.now() + Math.random() * 10,
      ...notification,
    });
  },
};

const actions = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
