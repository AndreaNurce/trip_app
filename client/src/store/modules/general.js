import ApiService from "../../services/api.service";
import TokenService from "../../services/token.service";

import router from "../../router/index";

const state = {
  tokenInfo : null
};

const getters = {
   getTokenInfo: state=> state.tokenInfo
};

const mutations = {
  async setTokenInfo(state) {
    state.tokenInfo = await TokenService.decodeToken()
  },
};

const actions = {
  async signUp({commit}, { type, payload }) {
    try {
      await ApiService.post(`/signup/${type}`, payload);
      router.push("/login");
    } catch (err) {
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
  async logIn({ commit }, { type, credentials }) {
    try {
      let res = await ApiService.post(`/login/${type}`, credentials);
      TokenService.setToken(res.data);
      ApiService.reInit();
      router.push(`/`);
    } catch (err) {
      console.log(err);
      commit(
        "notifications/addNotification",
        {
          type: "error",
          message: err?.response?.data?.message || "An error occoured",
        },
        { root: true }
      );
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
