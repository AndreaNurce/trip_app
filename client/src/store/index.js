import Vuex from 'vuex'
import general from "./modules/general"
import user from "./modules/user"
import agency from "./modules/agency"
import notifications from "./modules/notifications"


export default new Vuex.Store({
	modules: {
      general,
	  agency,
	  user,
	  notifications
	},
});