import { createRouter, createWebHistory } from "vue-router";
import TokenService from "../services/token.service";
import Home from "../views/Home.vue";
import User from "../views/User.vue";
import Agency from "../views/Agency.vue";
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true,
    },
    redirect: { name: "user" },
    children: [
      {
        path: "/user",
        name: "user",
        component: User,
        redirect: { name: "Trips" },
        meta: {
          requiresAuth: true,
          type: "user",
        },
        children: [
          {
            path: "",
            name: "Trips",
            component: () => import("../components/User/AllTrips.vue"),
            meta: {
              requiresAuth: true,
              type: "user",
            },
          },
          {
            path: "my-trips",
            name: "My Trips",
            component: () => import("../components/User/UserTrips.vue"),
            meta: {
              requiresAuth: true,
              type: "user",
            },
          },
        ],
      },
      {
        path: "/agency",
        name: "agency",
        component: () => import("../views/Agency.vue"),
        meta: {
          requiresAuth: true,
          type: "agency",
        },
        children: [
          {
            path: "",
            name: "AgencyTrips",
            component: () => import("../components/Agency/AgencyTrips.vue"),
            meta: {
              requiresAuth: true,
              type: "agency",
            },
          },
          {
            path: "trips",
            name: "AvailableTrips",
            component: () => import("../components/Agency/AvailableTrips.vue"),
            meta: {
              requiresAuth: true,
              type: "agency",
            },
          },
        ],
      },
    ],
  },
  {
    path: "/logIn",
    name: "LogIn",
    component: () => import("../views/LogIn.vue"),
  },
  {
    path: "/signUp",
    name: "SignUp",
    component: () => import("../views/SignUp.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach(async (to, from, next) => {
  if (to.meta.requiresAuth) {
    let token = await TokenService.decodeToken();
    if (token) {
      if (to.meta.type === token.data.type) {
        next();
      } else {
        next(`/${token.data.type}`);
      }
    } else {
      next("/login");
    }
  } else {
    next();
  }
});
export default router;
