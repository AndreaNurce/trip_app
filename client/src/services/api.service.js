import axios from "axios";
import TokenService from "./token.service";
import router from "../router/index";

const ApiService = {
  axios: axios.create(),
  init: function() {
    this.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
    this.axios.defaults.timeout = 15000;
    this.axios.defaults.headers.common[
      "Authorization"
    ] = TokenService.getToken();

    this.axios.interceptors.response.use(
      (respone) => respone,
      function(error) {
        if (401 === error.response.status) {
          this.logOut();
        } else {
          throw error;
        }
      }.bind(this)
    );
  },
  reInit() {
    this.axios.defaults.headers.common[
      "Authorization"
    ] = TokenService.getToken();
  },
  get: async function(URL, params) {
    return await this.axios.get(URL, params);
  },
  post: async function(URL, body = {}) {
    return await this.axios.post(URL, body);
  },
  delete: async function(URL, payload = {}) {
    return await this.axios.delete(URL, { data: payload });
  },
  put: async function(URL, payload = {}) {
    return await this.axios.put(URL, payload);
  },
  logOut: async function() {
    TokenService.removeToken();
    this.axios.defaults.headers.common["Authorization"] = null;
    router.push("/login");
  },
};

export default ApiService;
