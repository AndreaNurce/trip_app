import jwt_decode from "jwt-decode";

const TokenService = {
  setToken: function(token) {
    localStorage.setItem("token", token);
  },
  getToken() {
    return localStorage.getItem("token");
  },
  removeToken: async function() {
    localStorage.removeItem("token");
  },
  decodeToken: async function() {
    let token = this.getToken();
    if (token){
      let jwt = await jwt_decode(token);
      return jwt
    } 
  },
};

export default TokenService;
