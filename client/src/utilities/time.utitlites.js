module.exports = {
  timestampToDate(timestamp) {
    return new Date(timestamp).toLocaleDateString("en-US");
  },
  parseDate(date) {
    return Date.parse(date);
  },
};
