import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import ApiService from "./services/api.service";
import store from "./store";

ApiService.init();

createApp(App)
  .use(store)
  .use(router)
  .mount("#app");
